package com.marcotechnology.springbootdemowar;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class SpringbootdemowarController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello, World !! [WAR]" ;
    }
}