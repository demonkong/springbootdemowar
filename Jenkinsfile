 pipeline {
   parameters { 
        choice( name: 'DEPLOY_ENV', choices: ['DEV', 'SIT' , 'UAT'], description: 'Deploy to environment?')
    }
    agent any
    tools { 
        maven 'maven363' 
        jdk 'jdk14' 
    }

    stages {

      stage('Initial Environment'){
        steps {
                script {
                    //echo "${GIT_BRANCH}"
                    def prop = readProperties  file:'.env'
                    // prop.each{ k,v -> println "$k = $v" }
                    prop.each{ k,v -> env."${k}" = "${v}" }
                    MESSAGE_TEMPLATE = "$JOB_NAME Build #$BUILD_ID " + env['MESSAGE_TEMPLATE'] + "\r\n ${BUILD_URL}input"
                                                         
                }                
            }
      }
      stage('Build'){
          steps {
                sh 'mvn clean package'
                }
          }
      /*  stage ('Artifactory configuration') {
           steps{
                    script{
                        def pom = readMavenPom file: 'pom.xml'
                        groupId = pom.groupId
                        artifactId = pom.artifactId
                        version = pom.version
                        groupIdPath = groupId.replace(".","/")
                        artifactPath = "${groupIdPath}/${artifactId}/${version}"
                
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: JFROG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                        
                        sh "mv ${artifactPath}/${ORIGINAL_ARTIFACT_ID}-${ORIGINAL_ARTIFACT_VERSION}.jar ."
                        sh 'ls'
                       //ffffff
                    }
                } */
      stage('Code Quality Analysis') {
            steps {
                withSonarQubeEnv('sonarQlocal') {
                 sh 'mvn clean verify sonar:sonar -DskipTests'
                }
            }
          }
    
      stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                success{
                    echo "Sonar success"
                   
                }
                failure{
                    echo "Sonar failed"
                   
                }
            }
        }
        stage('Deploy'){
          steps {
                //echo "deploy adapters: [tomcat7(credentialsId: 'Tomcatadmin', path: '', url: 'http://centos.marcotechnology.com:8181')], contextPath: null, war: 'target/*.war'"
                deploy adapters: [tomcat9(credentialsId: '0b6aadbe-7d02-493e-a2c2-d979ad007574', path: '', url: "${DEV_HOST}")], contextPath: null, war: '**/*.war'
                }
                        }         
      stage('Robot Selenium Test (DEV)') {
            steps {
                script{
                    if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 1){
                        // Child Branch
                        branchName = env.GIT_BRANCH.replace("origin/","")
                        initialRobotFile("${ROBOT_DEV_BRANCH_FILE}","${DEV_BRANCH_HOST}")
            robotTest("${ROBOT_DEV_BRANCH_FILE}","DEV-${branchName}") // unknown branch

                    }else{
                        // Master
                        initialRobotFile("${ROBOT_DEV_FILE}","${DEV_HOST}")
            robotTest("${ROBOT_DEV_FILE}",'DEV') 
                    }
                }
            }
        }
      stage('JMeter Test (DEV)') {
            steps {
                script{
                    if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 1){
                        // Child Branch
                        branchName = env.GIT_BRANCH.replace("origin/","")
            initialJMeterFile("${JMETER_DEV_BRANCH_FILE}","${DEV_BRANCH_HOST}")
            jmeterTest("${JMETER_DEV_BRANCH_FILE}","DEV-${branchName}") // unknown branch
                    }else{
                        // Master
            initialJMeterFile("${JMETER_DEV_FILE}","${DEV_HOST}")
            jmeterTest("${JMETER_DEV_FILE}",'DEV') // master branch
                    }
                }
            }
        }
    }
    post { 
        always {
            // Publish Robot Report
            script {
                step(
                    [
                        $class              : 'RobotPublisher',
                        outputPath          : 'robot-result',
                        outputFileName      : "**/output.xml",
                        reportFileName      : '**/report.html',
                        logFileName         : '**/log.html',
                        disableArchiveOutput: false,
                        passThreshold       : 100,
                        unstableThreshold   : 95,
                        otherFiles          : "**/*.png,**/*.jpg",
                    ]
                )
            }
            deleteDir() /* clean up our workspace */
        }
        success{
            echo "Post all stage success"
            
        }
        failure{
            echo "Post all stage failed"
            
        }
    }
}    
        

def initialJMeterFile(fileName,host){
  script{
    dir("./jmeter-test"){
      def jmeter = readFile("${fileName}")
      jmeter = jmeter.replace("{JMETER_HOST}", "${host}")
      writeFile file:"${fileName}", text:jmeter
    }
  }
}

def initialRobotFile(fileName,host){
  script{
    dir("./robot-test"){
      def robot = readFile("${fileName}")
      robot = robot.replace("{ROBOT_HOST}", "http://" + "${host}")
      writeFile file:"${fileName}", text:robot
            sh "cat ${fileName}"
    }
  }
}
def robotTest(file,outputFolder){
    sh "mkdir -p robot-result/${outputFolder}" // Make Directory wait for result
    echo "${file}"
    echo "${outputFolder}"      
    build job: "${ROBOT_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./robot-result/${outputFolder}"){
        sh 'ls'
    }
}

def jmeterTest(file,outputFolder){
    sh "mkdir -p jmeter-result/${outputFolder}" // Make Directory wait for result
    build job: "${JMETER_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./jmeter-result/${outputFolder}"){
        sh 'ls'
        // Publish JMeter Report
        perfReport "jmeterResultTest-${outputFolder}.csv"
    }
}
